import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Observable} from "rxjs";
import {Causa} from "../../../models/causa";
import {CausaService} from "../../../services/causa.service";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {

  @ViewChild('modalAgregar', {static: false}) modalAgregar: ElementRef;
  causas: Observable<Causa[]>;
  selectedCausa: Causa;
  modal: NgbModalRef;

  constructor(private causaService: CausaService,
              private modalService: NgbModal,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.cargarCausas();
  }

  cargarCausas() {
    this.causas = this.causaService.listar();
  }

  abrirModal() {
    this.modal = this.modalService.open(this.modalAgregar);
  }

  cerrarModal($event) {
    if ($event) {
      this.cargarCausas();
      this.modal.close();
    }
  }

  editar(causa: Causa) {
    this.selectedCausa = Object.assign({}, causa);
    this.abrirModal();
  }

  eliminar(causa: Causa) {
    this.causaService.eliminar(causa).subscribe(data => {
      this.toastr.success("Registro eliminado");
      this.cargarCausas();
    });
  }

  agregar() {
    this.selectedCausa = null;
    this.abrirModal();
  }
}

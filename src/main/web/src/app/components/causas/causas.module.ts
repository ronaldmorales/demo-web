import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import {ReactiveFormsModule} from "@angular/forms";
import {ModalModule} from "ngb-modal";



@NgModule({
  declarations: [ListarComponent, AgregarComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ModalModule
  ]
})
export class CausasModule { }

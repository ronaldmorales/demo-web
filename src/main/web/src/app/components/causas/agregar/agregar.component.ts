import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CausaService} from "../../../services/causa.service";
import {ToastrService} from "ngx-toastr";
import {Causa} from "../../../models/causa";

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.scss']
})
export class AgregarComponent implements OnInit {

  causaForm: FormGroup;
  @Output()
  cerrarModal: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input()
  causa: Causa;

  constructor(private _fb: FormBuilder,
              private causaService: CausaService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.causaForm = this._fb.group({
      id: [this.causa && this.causa.id || null],
      nombre: [this.causa && this.causa.nombre || null, Validators.required]
    });
  }

  onSubmit(value: FormGroup) {
    if(value.valid) {
      if(this.causa && this.causa.id) {
        this.causaService.editar(value.value).subscribe(data => {
          if (data) {
            this.toastr.success("Registro Editado");
            this.cerrarModal.emit(true);
          }
        });
      } else {
        this.causaService.agregar(value.value).subscribe(data => {
          if (data) {
            this.toastr.success("Registro agregado");
            this.cerrarModal.emit(true);
          }
        });
      }
    } else {
      this.toastr.error( "Formulario inválido", "Error al agregar causa");
    }
  }
}
